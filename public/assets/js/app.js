
window.addEventListener('load', loader);

function loader() {

  const TLLOAD = gsap.timeline();

  TLLOAD
    .to('.images-container', { height: 400, duration: 1.3, delay: 0, ease: 'power2.out' })
    .to('.bloc-txt', { height: 'auto', duration: 0.6, ease: 'power2.out' }, '-=0.8')
    .to('.bloc-txt h2', { y: 0, ease: 'power2.out' }, '-=0.3')

    .to('.f2', { y: 0, duration: 0.6, ease: 'power2.out' })
    .add(() => {
      document.querySelector('.flip-img1').style.backgroundImage = "url('assets/images/img3demo.png')";
    })
    .to('.f2', { y: '-100%' })
    .to('.load-container', { opacity: 0, duration: 2.8, delay: 0.7 })
    .add(() => {
      document.querySelector('.load-container').style.display = "none";
    })
    .add(() => {
      document.querySelector('video').play()
    }, '-=0.7')
}

// animation scrollTrigger

function animateFrom(elem, direction) {
  direction = direction || 1;
  var x = 0,
    y = direction * 100;
  if (elem.classList.contains("gs_reveal_fromLeft")) {
    x = -100;
    y = 0;
  } else if (elem.classList.contains("gs_reveal_fromRight")) {
    x = 100;
    y = 0;
  }
  elem.style.transform = "translate(" + x + "px" + y + "px)";
  elem.style.opacity = "0";
  gsap.fromTo(elem, { x: x, y: y, autoAlpha: 0 }, {
    duration: 1.25,
    x: 0,
    y: 0,
    autoAlpha: 1,
    ease: "expo",
    overwrite: "auto"
  });
}

function hide(elem) {
  gsap.set(elem, { autoAlpha: 0 });
}

document.addEventListener("DOMContentLoaded", function () {
  gsap.registerPlugin(ScrollTrigger);

  gsap.utils.toArray(".gs_reveal").forEach(function (elem) {
    hide(elem); // assure that the element is hidden when scrolled into view

    ScrollTrigger.create({
      trigger: elem,
      onEnter: function () { animateFrom(elem) },
      onEnterBack: function () { animateFrom(elem, -1) },
      onLeave: function () { hide(elem) } // assure that the element is hidden when scrolled into view
    });
  });
});

window.visualViewport.addEventListener("zoom", viewportHandler);
function viewportHandler(event) {
  // NOTE: This doesn't actually work at time of writing
  if (event.target.scale > 3) {
    document.body.classList.remove("hide-text");
  } else {
    document.body.classList.add("hide-text");
  }
}


