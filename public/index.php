<?php

use App\Kernel;
use Symfony\Component\Cache\Adapter\FilesystemAdapter;
use Symfony\Component\Dotenv\Dotenv;
use Symfony\Component\ErrorHandler\Debug;
use Symfony\Component\HttpFoundation\Request;

require dirname(__DIR__).'/vendor/autoload.php';

(new Dotenv())->bootEnv(dirname(__DIR__).'/.env');

// $cache = new FilesystemAdapter();
// $cacheItem = $cache->getItem('user.account');

// if (!$cacheItem->isHit()) {
//     echo ' Miss! <br>';

//     $account = [
//         'name' => 'moi',
//         'id' => 12345
//     ];
    

//     $cacheItem->set($account);
//     $cache->save($cacheItem);
// } else {
//     echo ' Hit! <br>';
//     $account = $cacheItem->get();
// }
// dd($account);

if ($_SERVER['APP_DEBUG']) {
    umask(0000);

    Debug::enable();
}

$kernel = new Kernel($_SERVER['APP_ENV'], (bool) $_SERVER['APP_DEBUG']);
$request = Request::createFromGlobals();
$response = $kernel->handle($request);
$response->send();
$kernel->terminate($request, $response);
