<?php

namespace App\Controller;

use App\Classe\Search;
use App\Entity\Product;
use App\Form\SearchType;
use App\Repository\ProductRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class ProductController extends AbstractController
{
    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
    }

    /**
     * @Route("/nos-produits", name="products")
     */
    public function index(ProductRepository $repo, Request $request, EntityManagerInterface $em): Response
    {
        $search = new Search();
        $form = $this->createForm(SearchType::class, $search);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $search = $form->getData();
            $product = $repo->findWithSearch($search);
        } else {
            $product = $repo->findAll();
        }

        return $this->render('product/produits.html.twig', [
            'products' => $product,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/produit/{slug}", name="product")
     */
    public function show($slug, ProductRepository $repo): Response
    {
        $product = $repo->findOneBySlug($slug);
        //$product = $this->em->getRepository(Product::class)->findOneBySlug($slug);

        $products = $repo->findByIsBest(1);

        //dd($repo);
        //$products = $this->em->getRepository(Product::class)->findByIsBest(1);
        //dd($products);

        if (!$product) {
            return $this->redirectToRoute('products');
        }

        return $this->render('product/show.html.twig', [
            'product' => $product,
            'products' => $products,
        ]);
    }
}
