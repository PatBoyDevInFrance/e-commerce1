<?php

namespace App\Controller;

use App\Classe\Mail;
use App\Entity\User;
use App\Form\RegisterType;
use App\Repository\UserRepository;
use App\Security\LoginFormAuthentificatorAuthenticator;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\Security\Guard\GuardAuthenticatorHandler;

class RegisterController extends AbstractController
{
    // private $em;
    // public function __construct(EntityManagerInterface $em)
    // {
    //     $this->em = $em;
    // }

    /**
     * @Route("/inscription", name="register")
     */
    public function index(GuardAuthenticatorHandler $guardHandler, LoginFormAuthentificatorAuthenticator $authenticator, UserRepository $repo, Request $request, EntityManagerInterface $em, UserPasswordEncoderInterface $encoder): Response
    {
        $notification = null;
        $user = new User();
        $form = $this->createForm(RegisterType::class, $user);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $user = $form->getData();
            $password = $encoder->encodePassword($user, $user->getPassword());
            //dd($password);

            //$search_email = $em->getRepository(User::class)->findOneByEmail($user->getEmail());
            $search_email = $em->getRepository(User::class)->findOneBy(['email' => $user->getEmail()]);
            //$product_objet = $entityManager->getRepository(Product::class)->findOneBy(array('name' => $product->getProduct()));
            if (!$search_email) {
                $user->setPassword($password);
                $em->persist($user);
                $em->flush();
                // envoyer un mail
                $mail = new Mail();
                $content = 'Bonjour'.$user->getFirstName().'<br>Bienvenue sur la première boutique dédié au test <br><br>Bla bla bla ....';
                $mail->send($user->getEmail(), $user->getFirstName(), 'Bienvenue dans ma BoutiqueDEtest', $content);
                $notification = "Votre inscription s'est correctement déroulée. Vous pouvez dès à présent vous connecter à votre compte. ";
            } else {
                $notification = "L'email que vous avez renseigné existe déja !";
            }

            //return $this->redirectToRoute('register');
        }
        //remarque
        //$guardHandler->authenticateUserAndHandleSuccess( $user, $request, $authenticator,'main');
        //return $this->redirectToRoute('home');

        return $this->render('register/index.html.twig', [
            'form' => $form->createView(),
            'notification' => $notification,
        ]);
    }
}
