<?php

namespace App\Controller;

use App\Entity\Product;
use App\Repository\HeaderRepository;
use App\Repository\ProductRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class HomeController extends AbstractController
{
    /**
     * @Route("/", name="home")
     */
    public function index(EntityManagerInterface $entityManager, ProductRepository $repoProduct, HeaderRepository $headerRepo): Response
    {
        //phpinfo();
        $products = $entityManager->getRepository(Product::class)->findBy(['isBest' => true]);
        $prod = $repoProduct->findAll();

        //$products = $repoProduct->findBy(array('isBest' => true));

        $headers = $headerRepo->findAll();
        //dd($headers);

        //dd($products);
        return $this->render('home/anim.html.twig', [
            'prod' => $prod,
            'products' => $products,
            'headers' => $headers,
        ]);
    }
}
