<?php

namespace App\Controller;

use App\Classe\Cart;
use App\Classe\Mail;
use App\Classe\StockManagerService;
use App\Entity\Order;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class OrderSuccessController extends AbstractController
{
    /**
     * @Route("/commande/merci/{stripeSessionId}", name="order_validate")
     */
    public function index(Cart $carte, $stripeSessionId, EntityManagerInterface $em,StockManagerService $stockManager): Response
    {
        //$order = $em->getRepository(Order::class)->findOneByStripeSessionId($stripeSessionId);
        $order = $em->getRepository(Order::class)->findOneBy(['stripeSessionId' => $stripeSessionId]);
        if (!$order || $order->getUser() != $this->getUser()) {
            return $this->redirectToRoute('home');
        }

        // dd($order->getUser());

        if (0 == $order->getState()) {
            //vider la session "carte"
            $carte->removePanier();
            //destckage
            $stockManager->destock($order);
            // Modifier le statut isPaid de notre commande en mettant 1
            $order->setState(1);
            $em->flush();
            // Envoyer un email à notre client pour lui confirmer sa commande
            $mail = new Mail();
            $content = 'Bonjour '.$order->getUser()->getFirstName().'<br>Merci pour votre commande en raison des mesures de sécurité, nous vous préviendrons de chaque étape de votre commande, <br> Vous allez recevoir un mail quan votre commande sera en préparation puis un autre mail quand votre commande sera prête.<br> Merci et à tout de suite !';
            //.$order->getUser()->getOrderDetails()->getQuatity()->getPrice();
            $mail->send($order->getUser()->getEmail(), $order->getUser()->getFirstName(), 'Votre commande est bien validée', $content);

            //$mail->send($order->getUser()->getEmail(), $order->getUser()->getFirstName(),'Votre commande La BoutiqueDEtest est bien validée', $content);
        }

        // Afficher les quelques informations de la commande de l'utilisateur

        return $this->render('order_success/index.html.twig', [
        'order' => $order,
        ]);

        //dd($order);
    }
}
