<?php

namespace App\Controller;

use App\Classe\Cart;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class CartController extends AbstractController
{       
    // private $cartService;

    // public function __construct(Cart $cartService)
    // {
    //     $this->cartService = $cartService;
    // }

    /**
     * @Route("/mon-panier", name="cart")
     */
    public function index(Cart $cart): Response
    {   
        // $cart = $this->cartService->getFullCart();
        // if (!isset($cart['products'])) {
        //     return $this->redirectToRoute('home');
        // }
        return $this->render('cart/index.html.twig', [
            'cart' => $cart->getFullCart(),
            'total' => $cart->getTotal(),
            'totalQ' => $cart->getQuantity(),
        ]);
    }

    /**
     * @Route("/cart/add/{id}", name="add_to_cart")
     */
    public function add($id, Cart $cart): Response
    {
        $cart->add($id);

        return $this->redirectToRoute('cart');
    }

    /**
     * @Route("/cart/remove/{id}", name="remove_to_cart")
     */
    public function remove($id, Cart $cart): Response
    {
        $cart->remove($id);

        return $this->redirectToRoute('cart');
    }

    /**
     * @Route("/cart/delete/{id}", name="delete_to_cart")
     */
    public function delete(Cart $cart, $id)
    {
        $cart->remove($id);

        return $this->redirectToRoute('cart');
    }

    /**
     * @Route("/cart/decrease/{id}", name="decrease_to_cart")
     */
    public function decrease($id, Cart $cart): Response
    {
        $cart->decrease($id);

        return $this->redirectToRoute('cart');
    }
}
