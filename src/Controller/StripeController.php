<?php

namespace App\Controller;

use App\Classe\Cart;
use App\Entity\Order;
use App\Entity\Product;
use Doctrine\ORM\EntityManagerInterface;
use Stripe\Checkout\Session;
use Stripe\Stripe;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class StripeController extends AbstractController
{
    /**
     * @Route("/commande/create-session/{reference}", name="stripe_create_session")
     */
    public function index(EntityManagerInterface $entityManager, Cart $cart, $reference): Response
    {
        // créer un tableau pour pousser à Strype
        $products_for_stripe = [];
        // pas d'image car il n'y a pas de domaine
        //$YOUR_DOMAIN='http://localhost:8000';
        $YOUR_DOMAIN = 'https://clickncollec.herokuapp.com';

        //$order = $entityManager->getRepository(Order::class)->findOneByReference($reference);
        $order = $entityManager->getRepository(Order::class)->findOneBy(['reference' => $reference]);
        if (!$order) {
            new JsonResponse(['error' => 'order']);
        }

        //Enregistrer mes produits OrderDetails()
        foreach ($order->getOrderDetails()->getValues() as $product) {
            //$product_objet = $entityManager->getRepository(Product::class)->findOneByName($product->getProduct());
            $product_objet = $entityManager->getRepository(Product::class)->findOneBy(['name' => $product->getProduct()]);
            //$order = $entityManager->getRepository(Order::class)->findOneBy(array('reference' => $reference));
            $products_for_stripe[] = [
          'price_data' => [
              'currency' => 'eur',
              'unit_amount' => $product->getPrice(),
              'product_data' => [
                'name' => $product->getProduct(),
                'images' => [$YOUR_DOMAIN.'/uploads/'.$product_objet->getIllustration()],
             ],
         ],
         'quantity' => $product->getQuatity(),
        ];
        }

        $products_for_stripe[] = [
        'price_data' => [
         'currency' => 'eur',
         'unit_amount' => $order->getCarrierPrice(),
        'product_data' => [
           'name' => $order->getCarrierName(),
           'images' => [$YOUR_DOMAIN], // touver une icon pour le transporteur
         ],
     ],
     'quantity' => 1,
    ];

        Stripe::setApiKey('sk_test_51I4NqYLOWYwqA7Z8nqpou9teARAvPEaT4ICfDhuZKuSMCvORXAK8d16AkvVeB2pEbGLw764rUNFiJt3f5qXqPouK00P2oJz6pK');

        $checkout_session = Session::create([
      'customer_email' => $this->getUser()->getEmail(),
      'payment_method_types' => ['card'],
      'line_items' => [[
        $products_for_stripe,
      ]],
      'mode' => 'payment',
      'success_url' => $YOUR_DOMAIN.'/commande/merci/{CHECKOUT_SESSION_ID}',
      'cancel_url' => $YOUR_DOMAIN.'/commande/erreur/{CHECKOUT_SESSION_ID}',
    ]);

        $order->setStripeSessionId($checkout_session->id);
        $entityManager->flush();

        $response = new JsonResponse(['id' => $checkout_session->id]);

        return $response;
    }
}
