<?php

namespace App\Controller\Admin;

use App\Classe\Mail;
use App\Entity\Order;
use Doctrine\ORM\EntityManagerInterface;
use EasyCorp\Bundle\EasyAdminBundle\Config\Action;
use EasyCorp\Bundle\EasyAdminBundle\Config\Actions;
use EasyCorp\Bundle\EasyAdminBundle\Config\Crud;
use EasyCorp\Bundle\EasyAdminBundle\Context\AdminContext;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Field\ArrayField;
use EasyCorp\Bundle\EasyAdminBundle\Field\ChoiceField;
use EasyCorp\Bundle\EasyAdminBundle\Field\DateTimeField;
use EasyCorp\Bundle\EasyAdminBundle\Field\IdField;
use EasyCorp\Bundle\EasyAdminBundle\Field\MoneyField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextEditorField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;
use EasyCorp\Bundle\EasyAdminBundle\Router\CrudUrlGenerator;

class OrderCrudController extends AbstractCrudController
{
    private $crudUrlGenerator;
    private $entityManager;

    public function __construct(CrudUrlGenerator $crudUrlGenerator, EntityManagerInterface $entityManager)
    {
        $this->crudUrlGenerator = $crudUrlGenerator;
        $this->entityManager = $entityManager;
    }

    public static function getEntityFqcn(): string
    {
        return Order::class;
    }

    public function configureActions(Actions $actions): Actions
    {
        $updatePreparation = Action::new('updatePreparation', '<-préparation en cours---', 'fas fa-box-open')->linkToCrudAction('updatePreparation');
        $updateDelivery = Action::new('updateDelivery', '<-Commande PRÊTE---', 'fas fa-truck')->linkToCrudAction('updateDelivery');
        $updateShippin = Action::new('updateShippin', '<-livré---','fas fa-user' )->linkToCrudAction('updateShippin');
        return $actions
        ->add('detail', $updatePreparation)
        ->add('detail', $updateDelivery)
        ->add('detail', $updateShippin)
        ->add('index', 'detail');
    }

    public function updatePreparation(AdminContext $context)
    {
        //die('ok');
        $order = $context->getEntity()->getInstance();
        $order->setState(2);
        $this->entityManager->flush();

        $this->addFlash('notice', "<span style='color:green;'><strong>La commande".$order->getReference().'est bien <u>en cours de préparation <u/>.</strong></span> ');

        $url = $this->crudUrlGenerator->build()
            ->setController(OrderCrudController::class)
            ->setAction('index')
            ->generateUrl();

        // rajouter ici l'email pour prévenir le client que la commande est en cours de prépa
        $mail = new Mail();
        $content = 'Bonjour '.$order->getUser()->getFirstName().'<br>Votre commande est préparation. <br><br>Vous serez prévenu des quelle sear prête ....';
        //dd($mail->send($order->getUser()->getEmail(),$order->getUser()->getFirstName(),'Votre commande La BoutiqueDEtest est bien validée', $content));
        //.$order->getUser()->getOrderDetails()->getQuatity()->getPrice();
        $mail->send($order->getUser()->getEmail(), $order->getUser()->getFirstName(), 'Votre commande est en préparation', $content);
        //$mail->send($order->getUser()->getEmail(), $order->getUser()->getFirstName(),'Votre commande La BoutiqueDEtest est bien validée', $content);
        return $this->redirect($url);
    }

    public function updateDelivery(AdminContext $context)
    {
        //die('ok');
        $order = $context->getEntity()->getInstance();
        $order->setState(3);
        $this->entityManager->flush();

        $this->addFlash('notice', "<span style='color:orange;'><strong>La commande".$order->getReference().'est <u>en cours de livraison <u/>.</strong></span> ');

        $url = $this->crudUrlGenerator->build()
            ->setController(OrderCrudController::class)
            ->setAction('index')
            ->generateUrl();
        // rajouter ici l'email pour prévenir le client que la commande est en cours de prépa
        $mail = new Mail();
        $content = 'Bonjour '.$order->getUser()->getFirstName().'<br>Génial !! <br>Votre commande est prête. <br><br>Vous pouvez venir la chercher. <br> à tout de suite.';
        $mail->send($order->getUser()->getEmail(), $order->getUser()->getFirstName(), 'Géniale ! Votre commande est prête', $content);

        return $this->redirect($url);
    }

    public function updateShippin(AdminContext $context){
        $order = $context->getEntity()->getInstance();
        $order->setState(4);
        $this->entityManager->flush();
        $this->addFlash('notice', "<span style='color:green;'><strong>La commande".$order->getReference().'est livré <u>ou récupéré <u/>.</strong></span> ');

        $url = $this->crudUrlGenerator->build()
            ->setController(OrderCrudController::class)
            ->setAction('index')
            ->generateUrl();
        $mail = new Mail();
        $content = 'Bonjour '.$order->getUser()->getFirstName().'<br>Magnifique !! <br>Votre commande à bien été livrée. <br><br>Merci et à très bientôt.<br>';
        $mail->send($order->getUser()->getEmail(), $order->getUser()->getFirstName(), 'Magnifique ! Votre commande est livrée', $content);


        return $this->redirect($url);
    }

    public function configureCrud(Crud $crud): Crud
    {
        return $crud->setDefaultSort(['id' => 'DESC']);
    }

    public function configureFields(string $pageName): iterable
    {
        return [
            IdField::new('id'),
            ChoiceField::new('state')->setChoices([
                '<span style="color:red">Non payée </span>' => 0,
                '<span style="color:green" >Payée </span>' => 1,
                '<span style="color:blue" >Préparation en cours </span>' => 2,
                'Livraison en cours' => 3,
                '<span style="color:orange"><b>Livré</span>' => 4,
            ]),
            TextField::new('user.getFullName', 'Utilisateur'),
            DateTimeField::new('createdAt', 'Passé le'),
            TextEditorField::new('delivery', 'Adresse de livraison')->onlyOnDetail(),
            TextField::new('carrierName', 'Transporteur'),
            MoneyField::new('carrierPrice', 'Frais de port')->setCurrency('EUR'),
            MoneyField::new('total', 'Total sans livraison')->setCurrency('EUR'),
            ArrayField::new('orderDetails', 'Produits achetés')->hideOnIndex(), //pour masquer
            //les produits achetés
        ];
    }
}
