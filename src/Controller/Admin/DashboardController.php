<?php

namespace App\Controller\Admin;

use App\Entity\Carrier;
use App\Entity\Category;
use App\Entity\Header;
use App\Entity\Order;
use App\Entity\Product;
use App\Entity\User;
use App\Repository\UserRepository;
use EasyCorp\Bundle\EasyAdminBundle\Config\Assets;
use EasyCorp\Bundle\EasyAdminBundle\Config\Dashboard;
use EasyCorp\Bundle\EasyAdminBundle\Config\MenuItem;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractDashboardController;
use EasyCorp\Bundle\EasyAdminBundle\Router\CrudUrlGenerator;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class DashboardController extends AbstractDashboardController
{   
   
    /**
     * @Route("/admin", name="admin")
     */
    public function index(): Response
    {
        $routeBuilder = $this->get(CrudUrlGenerator::class)->build();

        return $this->redirect($routeBuilder->setController(OrderCrudController::class)->generateUrl());

        
    }


    /**
     * @Route("/admin", name="home")
     */
    public function configureTitle()
    {
        return $this->redirectToRoute('home/index.html.twig');
    }

    public function configureDashboard(): Dashboard
    {
        return Dashboard::new()
            ->setTitle('Ecommerce-Ma boutique Test');
    }

    public function configureMenuItems(): iterable
    {
        yield MenuItem::linkToUrl('retour sur le site', 'fas fa-home', 'https://clickncollec.herokuapp.com')->setLinkTarget('_blank')->setLinkRel('noreferrer');
        //yield MenuItem::linkToCrud('Nb visiteurs', 'fas fa-users', Cart::class);
        yield MenuItem::linkToCrud('Utilisateur', 'fas fa-user', User::class);
        yield MenuItem::linkToCrud('Commandes', 'fas fa-shopping-cart', Order::class);   
        yield MenuItem::linkToCrud('Produits', 'fas fa-tags', Product::class);
        yield MenuItem::linkToCrud('Categories', 'fas fa-list', Category::class);
        yield MenuItem::linkToCrud('Livreurs', 'fas fa-truck', Carrier::class);
        yield MenuItem::linkToCrud('Header', 'fas fa-desktop', Header::class);
    }

    public function configureAssets(): Assets
    {
        return Assets::new()

            ->addHtmlContentToHead('<style>:root { --color-primary: #123456; }</style>');
    }
}
