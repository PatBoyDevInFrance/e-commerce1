<?php

namespace App\Controller;

use App\Classe\Mail;
use App\Entity\ResetPassword;
use App\Form\ResetPasswordType;
use App\Repository\ResetPasswordRepository;
use App\Repository\UserRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class ResetPasswordController extends AbstractController
{
    /**
     * @Route("/mot-de-passe-oublie", name="reset_password")
     */
    public function index(Request $request, UserRepository $userRepo, EntityManagerInterface $em): Response
    {
        if ($this->getUser()) {
            return $this->redirectToRoute('home');
        }

        if ($request->get('email')) {
            // étape 1 :
            // Enregistrer en BD la demande de reset_password avec user, token, createdAt
            $user = $userRepo->findOneByEmail($request->get('email'));
            if ($user) {
                $reset_password = new ResetPassword();
                $reset_password->setUser($user);
                $reset_password->setToken(uniqid());
                $reset_password->setCreatedAt(new \DateTime());
                $em->persist($reset_password);
                //dd($reset_password);
                $em->flush();

                // envoyer un email à utilisateur avec un lien lui permettant de mettre à jour son
                // mot de passe.
                $url = $this->generateUrl('update_password', [
                    'token' => $reset_password->getToken(),
                ]);
                $content = 'Bonjour '.$user->getFirstname().'<br>Voas avez demandé à réinitialiser votre mot de passe sur le site La BoutiqueDeTest <br><br>';
                $content .= "Merci de bien vouloir cliquer sur le lien suivant pour <a href='".$url."' >  mettre à jour votre mot de passe </a>.";

                $mail = new Mail();
                $mail->send($user->getEmail(), $user->getFirstname().' '.$user->getLastname(), 'réinitialiser votre mot de passe sur la BoutiqueDeTest', $content);
                $this->addFlash('notice', 'Vous allez recevoir dans quelques secondes un mail avec la procédure pour réinitialiser votre  mot de passe.');
            } else {
                $this->addFlash('notice', 'Cette adresse email est inconnue.');
            }
        }

        return $this->render('reset_password/index.html.twig');
    }

    /**
     * @Route("/modifier-mon-mot-de-passe/{token}", name="update_password")
     */
    public function update($token, ResetPasswordRepository $resetPasswordRepo, EntityManagerInterface $em, Request $request, UserPasswordEncoderInterface $encoder): Response
    {
        $reset_password = $resetPasswordRepo->findOneByToken($token);

        if (!$reset_password) {
            return $this->redirectToRoute('reset_password');
        }
        // vérifier si le createdAt = now - 3h
        // créer un test unitaire
        $now = new \DateTime();

        if ($now > $reset_password->getCreatedAt()->modify('+ 3 hour')) {
            //die('ça fonctionne ! ');
            $this->addFlash('notice', 'Votre demande de mot de passe a expiré. Merci de la renouveller');

            return $this->redirectToRoute('reset_password');
        }

        // dump($now);
        // dump($reset_password->getCreatedAt()->modify('+ 3 hour'));
        // dd($reset_password);

        // Rendre une vue avec mot de passe et confirmez votre mot de passe
        $form = $this->createForm(ResetPasswordType::class);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $new_pwd = $form->get('new_password')->getData();
            //dd($form->getData());
            // encodage des mot de passe
            $password = $encoder->encodePassword($reset_password->getUser(), $new_pwd);
            $reset_password->getUser()->setPassword($password);
            //$em->persist($reset_password);
            // Flush en BD
            $em->flush();
            // Redirection de l'utilisateur vers la page de connexion
            $this->addFlash('notice', 'Votre mot de passe à bien été mis à jour.');

            return $this->redirectToRoute('app_login');
        }

        return $this->render('reset_password/update.html.twig', [
            'form' => $form->createView(),
        ]);
    }
}
