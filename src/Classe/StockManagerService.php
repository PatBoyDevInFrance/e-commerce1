<?php 

namespace App\Classe;

use App\Entity\Order;
use App\Entity\OrderDetails;
use App\Repository\ProductRepository;
use Doctrine\ORM\EntityManagerInterface;

class StockManagerService {

    private $manager;
    private $repoProduct;


    public function __construct(EntityManagerInterface $manager,ProductRepository $repoProduct)
    {
        $this->manager = $manager;
        $this->repoProduct =$repoProduct;
    }

    public function destock(Order $order){
        $orderDetails = $order->getOrderDetails()->getValues();
        foreach($orderDetails as $key=> $details){
            $product = $this->repoProduct->findByName($details->getProduct())[0];
            $newQuantity = $product->getQuantity() - $details->getQuatity();
            $product->setQuantity($newQuantity);
            $this->manager->flush();
        }

    }

}