<?php

namespace App\Classe;

use Mailjet\Client;
use Mailjet\Resources;

class Mail
{
    private $api_key = 'ff7323264942a8901a1f7df4c339b6d8';
    private $api_key_secret = 'e5d53eaf95576be21daa49a85ddd2a08';

    public function send($to_email, $to_name, $subject, $content)
    {
        $mj = new client($this->api_key, $this->api_key_secret, true, ['version' => 'v3.1']);
        $body = [
            'Messages' => [
                [
                    'From' => [
                        'Email' => 'bretagne.sensations@gmail.com',
                        'Name' => 'La Boutique de Test',
                    ],
                    'To' => [
                        [
                            'Email' => $to_email,
                            'Name' => $to_name,
                        ],
                        [
                            'Email' => 'bretagne.sensations@gmail.com',
                        ],
                    ],
                    'TemplateID' => 2363924,
                    'TemplateLanguage' => true,
                    'Subject' => $subject,
                    'Variables' => [
                        'content' => $content,
                    ],
                ],
            ],
        ];
        $response = $mj->post(Resources::$Email, ['body' => $body]);
        $response->success();
    }
}
