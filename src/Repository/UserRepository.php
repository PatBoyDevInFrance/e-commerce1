<?php

namespace App\Repository;

use App\Entity\User;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;
use League\OAuth2\Client\Provider\GoogleUser;
use Symfony\Component\Security\Core\Exception\UnsupportedUserException;
use Symfony\Component\Security\Core\User\PasswordUpgraderInterface;
use Symfony\Component\Security\Core\User\UserInterface;

/**
 * @method User|null find($id, $lockMode = null, $lockVersion = null)
 * @method User|null findOneBy(array $criteria, array $orderBy = null)
 * @method User[]    findAll()
 * @method User[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class UserRepository extends ServiceEntityRepository implements PasswordUpgraderInterface
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, User::class);
    }

    /**
     * Used to upgrade (rehash) the user's password automatically over time.
     */
    public function upgradePassword(UserInterface $user, string $newEncodedPassword): void
    {
        if (!$user instanceof User) {
            throw new UnsupportedUserException(sprintf('Instances of "%s" are not supported.', \get_class($user)));
        }

        $user->setPassword($newEncodedPassword);
        $this->_em->persist($user);
        $this->_em->flush();
    }

    public function findOrCreateGoogleFromOauth(GoogleUser $owner): User
    {
        /** @var User/null $user */
        $user = $this->createQueryBuilder('u')
            ->where('u.googleId = :googleId')
            ->orWhere('u.email =:email')
            ->setParameters([
                'email' => $owner->getEmail(),
                'googleId' => $owner->getId(),
            ])
            ->getQuery()
            ->getOneOrNullResult();

        if ($user) {
            if (null === $user->getGoogleId()) {
                $user->setGoogleId($owner->getId());
                $this->getEntityManager()->flush();
            }

            return $user;
        }
        $user = (new User())
            ->setRoles(['ROLE_USER'])
            ->setGoogleId($owner->getId())
            ->setEmail($owner->getEmail())
            //->setUserName($owner->getName())
            ->setLastName($owner->getLastName())
            ->setFirstname($owner->getFirstName());
        //->setFullname($owner->getId());
        $em = $this->getEntityManager();
        $em->persist($user);
        $em->flush();

        return $user;
    }

    /**
     * @return int|mixed|string
     */
    public function countAllUser(){
        $queryBuilder = $this->createQueryBuilder('a');
        $queryBuilder->select('COUNT(a.id) as value' );
        return $queryBuilder->getQuery()->getResult();
    }
}
